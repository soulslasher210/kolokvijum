import { Injectable } from '@angular/core';
import { Zadatak } from '../../../zadatak';
import { HttpClient,  } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: {
    'Content-Type': 'application/json'
  }
}



@Injectable({
  providedIn: 'root'
})
export class ServisiService {
  zadatakUrl:string = 'http://localhost:8080/';


  constructor(private http:HttpClient) { }

//Dobavljanje podataka

getPodatak():Observable<Zadatak[]> {
  return this.http.get<Zadatak[]>(`${this.zadatakUrl}`);
}

//Brisanje podataka
deletePodatak(zadatak:Zadatak):Observable<Zadatak> {
  const url = `${this.zadatakUrl}/${zadatak.id}`;
  return this.http.delete<Zadatak>(url, httpOptions);
}

// Add Todo
addPodatak(zadatak:Zadatak):Observable<Zadatak> {
  return this.http.post<Zadatak>(this.zadatakUrl, zadatak, httpOptions);
}




}
