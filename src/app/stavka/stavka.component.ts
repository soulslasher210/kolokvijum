import { Component, OnInit ,Input,EventEmitter,Output} from '@angular/core';
import { ServisiService } from '../servisi/servisi.service';
import { Zadatak } from '../../../zadatak';

@Component({
  selector: 'app-stavka',
  templateUrl: './stavka.component.html',
  styleUrls: ['./stavka.component.css']
})
export class StavkaComponent implements OnInit {

  @Input()zadatak:Zadatak;
  @Output()deletePodatak:EventEmitter<Zadatak> = new EventEmitter();

  constructor(private servis:ServisiService) { }

  ngOnInit(): void {
  }

  onDelete(zadatak) {
    this.deletePodatak.emit(zadatak);
  }



}
