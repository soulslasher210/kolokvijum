import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StavkeComponent } from './stavke/stavke.component';


const routes: Routes = [
  { path: '', component: StavkeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
