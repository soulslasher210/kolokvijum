import { Component, OnInit,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-dodaj-podatak',
  templateUrl: './dodaj-podatak.component.html',
  styleUrls: ['./dodaj-podatak.component.css']
})
export class DodajPodatakComponent implements OnInit {
@Output() dodajPodatak: EventEmitter<any> = new EventEmitter();

  naziv:string;
  skracenica:string;

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit() {
    const todo = {
      naziv: this.naziv,
      skracenica: this.skracenica
    }

    this.dodajPodatak.emit(todo);
  }

}
