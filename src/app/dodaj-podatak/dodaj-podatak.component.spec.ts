import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DodajPodatakComponent } from './dodaj-podatak.component';

describe('DodajPodatakComponent', () => {
  let component: DodajPodatakComponent;
  let fixture: ComponentFixture<DodajPodatakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DodajPodatakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DodajPodatakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
