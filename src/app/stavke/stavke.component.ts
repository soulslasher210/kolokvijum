import { Component, OnInit } from '@angular/core';
import { ServisiService } from '../servisi/servisi.service';
import { Zadatak } from '../../../zadatak';

@Component({
  selector: 'app-stavke',
  templateUrl: './stavke.component.html',
  styleUrls: ['./stavke.component.css']
})
export class StavkeComponent implements OnInit {
  zadatak:Zadatak[];


  constructor(private servis:ServisiService) { }

  ngOnInit() {
    this.servis.getPodatak().subscribe(zadatak =>{
      this.zadatak = zadatak;
    })
  }


  deletePodatak(zadatak:Zadatak) {
    // Remove From UI
    this.zadatak = this.zadatak.filter(t => t.id !== zadatak.id);
    // Remove from server
    this.servis.deletePodatak(zadatak).subscribe();
  }

  addPodatak(zadatak:Zadatak) {
    this.servis.addPodatak(zadatak).subscribe(zadatak => {
      this.zadatak.push(zadatak);
    });
}
}
