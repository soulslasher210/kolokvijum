const http = require('http');

const hostname = '127.0.0.1';
const port = 8080;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  var jsonFajl = {
       "predmet": [
          {
             "id": 1,
             "naziv": "Matematika",
             "skracenica": "mat"
          },
          {
             "id": 2,
             "naziv": "Fizika",
             "skracenica": "fiz"
          },
          {
             "id": 3,
             "naziv": "Hemija",
             "skracenica": "hem"
          },
          {
             "id": 4,
             "naziv": "Engleski jezik",
             "skracenica": "eng"
          }
       ],
       "ucenik": [
          {
             "id": 1,
             "ime": "Marko",
             "prezime": "Petrovic"
          },
          {
             "id": 2,
             "ime": "Jovana",
             "prezime": "Jovanović"
          },
          {
             "id": 3,
             "ime": "Nikola",
             "prezime": "Petrovic"
          },
          {
             "id": 4,
             "ime": "Marko",
             "prezime": "Petrovic"
          }
       ],
       "ocena": [
          {
             "id": 1,
             "ocena": 3,
             "predmetId": 1,
             "ucenikId": 3,
             "datumOcene": "2020-01-06"
          },
          {
             "id": 2,
             "ocena": 4,
             "predmetId": 2,
             "ucenikId": 1,
             "datumOcene": "2020-02-16"
          },
          {
             "id": 3,
             "ocena": 2,
             "predmetId": 2,
             "ucenikId": 2,
             "datumOcene": "2020-02-03"
          },
          {
             "id": 4,
             "ocena": 1,
             "predmetId": 1,
             "ucenikId": 1,
             "datumOcene": "2020-03-26"
          }
       ]

  }
  res.end(JSON.stringify(jsonFajl));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
